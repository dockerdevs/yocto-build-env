# Yocto Build Environment #

This repo provies a build environment for building your yocto projects. The build
environment is provided using vagrant with virtualbox as the virtualization
provider. All the tools for various yocto versions are already present. The
only thing left to the user is to configure her git credentials and start cloing.
A project directory is mapped and ssh-agents and ssh_x agents are forwarded by default.

The yocto 1.8 box is also hosted on [Hashicorp atlas](https://atlas.hashicorp.com/wolverine2k/boxes/yocto-1.8) and can be used directly using:

>`vagrant init wolverine2k/yocto-1.8; vagrant up --provider virtualbox`

Other boxes will also be uploaded there as soon as they are ready.


### Contribution guidelines ###

You are welcome to contribute to this repo. Please make sure that the box is tested properly
and verbose comments in your patches would help me a lot.

Shoot me an email in any case if needed.

Follow me on Twitter @wolverine2k

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Changelog ###

1. Version: 0.0.1 - 27th April 2017
> * Yocto 1.8 box released
> * Initial release of the codebase on bitbucket :)