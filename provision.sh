apt-get -y update

### To make the box smaller, many support packages are not included by default.

### Uncomment the below line to include the documentation packages as well.
#apt-get -y install xsltproc docbook-utils fop dblatex xmlto

### Uncomment the below line to include Open Embedded Self-test support
#apt-get -y install python-git

apt-get -y install gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential chrpath socat cpio python python3 python-pip python-pexpect libsdl1.2-dev xterm make nano libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev vim npm ssh git curl wget

curl http://downloads.yoctoproject.org/releases/yocto/yocto-2.2.1/buildtools/x86_64-buildtools-nativesdk-standalone-2.2.1.sh -o x86_64-buildtools-nativesdk-standalone-2.2.1.sh
chmod +x x86_64-buildtools-nativesdk-standalone-2.2.1.sh
sh ./x86_64-buildtools-nativesdk-standalone-2.2.1.sh -y -d /home/vagrant/buildtools
rm x86_64-buildtools-nativesdk-standalone-2.2.1.sh

### Install required NodeJs 4.8.0 & Meteor
npm install -g n
n 4.8.0
ln -sf /usr/local/n/versions/node/4.8.0/bin/node /usr/bin/node
curl https://install.meteor.com/ | sh

### Install repo & append to path
mkdir /home/vagrant//bin
echo "export PATH=/home/vagrant/bin:$PATH" >> .bashrc
echo "export TERM=${TERM:-dumb}" >> .bashrc
curl https://storage.googleapis.com/git-repo-downloads/repo > /home/vagrant/bin/repo
chmod a+x /home/vagrant/bin/repo

### Now the box is ready